<div align="center">

![Logo](https://gitea.com/CatsPnewed/Starlight-UI/raw/commit/4bd0d6f48ef04b1f842de8f12c5153468e3c1905/Icons/launch.png)

### Starlight Launcher UI

### About Project

Utility to use Starlight with UI. 
When me back, I will add source and update it soon.

### Credits
 to Icon8 for original exe icon
 to RealNickk for Starlight Launcher

### Code License

It is BSD 3 Clause, read it [there](https://gitea.com/CatsPnewed/Starlight-UI/raw/commit/4bd0d6f48ef04b1f842de8f12c5153468e3c1905/LICENSE)

</div>